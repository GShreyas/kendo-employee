import { RouterModule } from "@angular/router";
import { ButtonGroup, ButtonsModule } from "@progress/kendo-angular-buttons";
import { GridModule } from "@progress/kendo-angular-grid";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule, LOCALE_ID } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { EmployeeModule } from "./employee/employee.module";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { WindowModule, DialogsModule } from "@progress/kendo-angular-dialog";
import { LayoutModule } from "@progress/kendo-angular-layout";
import { NotificationModule } from "@progress/kendo-angular-notification";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { UploadModule } from "@progress/kendo-angular-upload";

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    EmployeeModule,
    HttpClientModule,
    DropDownsModule,
    BrowserAnimationsModule,
    GridModule,
    ButtonsModule,
    WindowModule,
    DialogsModule,
    RouterModule,
    LayoutModule,
    NotificationModule,
    DateInputsModule,
    UploadModule,
  ],
  providers: [
    {
      // Set default locale to bg-BG
      provide: LOCALE_ID,
      useValue: "en-AU",
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
