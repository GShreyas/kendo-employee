import { Component, Injectable, ViewEncapsulation } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

import { Employee } from "./employee";
import { NotificationService } from "@progress/kendo-angular-notification";

@Component({
  selector: "my-app",
  template: "",
  styles: [
    `
      .button-notification {
        padding: 10px 5px;
        color: red;
        width: 100px;
        height: 100px;
      }
    `,
  ],
  encapsulation: ViewEncapsulation.None,
})
@Injectable({
  providedIn: "root",
})
export class EmployeeService {
  // private apiURL = "https://my-json-server.typicode.com/ShreyasGanvkar/MyJSONdata";

  // private apiURL = "https://api.jsonbin.io/b/60be2b5a92164b68bec39a2e";

  private apiURL = "http://localhost:3000";

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
    }),
  };

  constructor(
    private httpClient: HttpClient,
    private notificationService: NotificationService
  ) {}

  getAll(): Observable<Employee[]> {
    return this.httpClient
      .get<Employee[]>(this.apiURL + "/employees/")
      .pipe(catchError(this.errorHandler));
  }

  create(post): Observable<Employee> {
    // console.error("EmployeeService:create()",post);
    return this.httpClient
      .post<Employee>(this.apiURL + "/employees/", post)
      .pipe(catchError(this.errorHandler));
  }

  find(id): Observable<Employee> {
    // console.error("EmployeeService:find():id",id);
    return this.httpClient
      .get<Employee>(this.apiURL + "/employees/" + id)
      .pipe(catchError(this.errorHandler));
  }

  update(id, post): Observable<Employee> {
    return this.httpClient
      .put<Employee>(this.apiURL + "/employees/" + id, post)
      .pipe(catchError(this.errorHandler));
  }

  delete(id) {
    return this.httpClient
      .delete<Employee>(this.apiURL + "/employees/" + id)
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

  //Sucess Notification
  public showSuccess(text): void {
    this.notificationService.show({
      cssClass: "button-notification",
      content: text,
      hideAfter: 2000,
      position: { horizontal: "center", vertical: "top" },
      animation: { type: "fade", duration: 1000 },
      type: { style: "success", icon: true },
    });
  }
}
