export interface Employee {
  id: number;
  empFname: string;
  empLname: string;
  empDOB: string;
  empSal: number;
  empImg: string;
  deptID: string;
}
