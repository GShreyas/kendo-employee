import {
  Component,
  OnInit,
  Inject,
  LOCALE_ID,
  Input,
  HostListener,
  EventEmitter,
  Output,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";

import {
  DragResizeService,
  WindowComponent,
} from "@progress/kendo-angular-dialog";
import { FileRestrictions } from "@progress/kendo-angular-upload";
import { IntlService } from "@progress/kendo-angular-intl";
import { offset } from "@progress/kendo-popup-common";

import { EmployeeService } from "../employee.service";
import { Employee } from "../employee";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.css"],
})
export class EditComponent implements OnInit {
  employee?: Employee;
  empDOB;
  ESC_KEY = 27;
  width = "350";
  height = "615";
  minHeight = "210";
  minWidth = "200";
  title = "Update employee details";

  @Output() close: EventEmitter<any> = new EventEmitter();

  @Input() empID = "";

  //Departments list
  public departmentList: Array<{ text: string; value: string }> = [
    { text: "Marketing", value: "Marketing" },
    { text: "Sales", value: "Sales" },
    { text: "IT", value: "IT" },
    { text: "Security", value: "Security" },
    { text: "Administration", value: "Administration" },
  ];

  // Images file upload restriction
  public FileTypeRestrictions: FileRestrictions = {
    allowedExtensions: [".jpg", ".png"],
    maxFileSize: 2000000,
    minFileSize: 5000,
  };
  base64textString: any;

  @ViewChild(WindowComponent, { static: false })
  windowComponent: WindowComponent;

  /**
   * Centers Kendo-Window to the middle of the screen
   */
  @HostListener("window:scroll", [])
  @HostListener("window:resize", [])
  centerWindow() {
    const windowService = this.windowComponent["service"] as DragResizeService;

    if (!windowService) {
      return;
    }
    if (windowService.options.state === "maximized") {
      return;
    }

    const wnd = windowService.windowViewPort;
    const wrapper = offset(windowService.window.nativeElement);

    const top = Math.max(0, (wnd.height - wrapper.height) / 2);
    const left = Math.max(0, (wnd.width - wrapper.width) / 2);

    windowService.options.position = "fixed";
    this.windowComponent.setOffset("top", top);
    this.windowComponent.setOffset("left", left);
  }

  @HostListener("keydown", ["$event"])
  public onComponentKeydown(event: KeyboardEvent): void {
    // don't fix .keyCode to keep IE11 support
    // tslint:disable-next-line: deprecation
    if (event.keyCode === this.ESC_KEY) {
      this.windowComponent.close.emit();
    }
  }

  constructor(
    public employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router,
    private intl: IntlService,
    @Inject(LOCALE_ID) public locale: string
  ) {}

  ngOnInit(): void {
    this.employeeService.find(this.empID).subscribe((data: Employee) => {
      // console.error("EditComponent:ngOnInit():data",data);
      this.employee = data;
      this.empDOB = data.empDOB; //

      // console.error("EditComponent:ngOnInit():this.empDOB", this.empDOB);
    });
  }

  public form: FormGroup = new FormGroup({
    id: new FormControl("", [Validators.required]),
    empFname: new FormControl("", [
      Validators.required,
      Validators.pattern("^[A-Za-z ]+$"),
    ]),
    empLname: new FormControl("", [
      Validators.required,
      Validators.pattern("^[A-Za-z ]+$"),
    ]),
    empDOB: new FormControl(new Date(), [Validators.required]),
    empSal: new FormControl("", [Validators.required]),
    empImg: new FormControl("", [Validators.required]),
    deptID: new FormControl("", [Validators.required]),
  });

  get f() {
    return this.form.controls;
  }

  handleFileSelect(evt) {
    console.error("handleFileSelect(evt)");
    var files = evt;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    // console.error("readerEvt:");
    var binaryString = readerEvt.target.result;
    // console.error("readerEvt.target.result:");
    this.base64textString = btoa(binaryString);
    // console.error("encodedData:");
    this.form.value.empImg = this.base64textString;

    let date = JSON.stringify(this.form.value.empDOB);
    this.form.value.empDOB = JSON.parse(date).substring(0, 10);

    this.employeeService
      .update(this.empID, this.form.value)
      .subscribe((res) => {
        console.log("--Employee updated successfully!--");
        this.windowComponent.close.emit();
        this.employeeService.showSuccess("Employee updated successfully!");
      });
  }

  onSubmit() {
    // console.error("EditComponent:onSubmit()");
    // console.error("-------------------", this.form.controls.empImg.dirty);
    // console.error("-------------------", this.form.controls.empImg.touched);
    if (this.form.invalid) {
      console.error("invalid");
      this.form.markAllAsTouched();
      return;
    }

    if (this.form.controls.empImg.dirty || this.form.controls.empImg.touched) {
      this.handleFileSelect(this.form.value.empImg);
    } else {
      this.employeeService
        .update(this.empID, this.form.value)
        .subscribe((res) => {
          console.log("Employee updated successfully!");
          this.windowComponent.close.emit();
          this.employeeService.showSuccess("Employee updated successfully!");
        });
    }
  }
}
