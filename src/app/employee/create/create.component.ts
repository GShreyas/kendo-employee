import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { EventEmitter, Output, HostListener } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { FileRestrictions } from "@progress/kendo-angular-upload";
import { offset } from "@progress/kendo-popup-common";
import {
  WindowComponent,
  DragResizeService,
} from "@progress/kendo-angular-dialog";

import { Employee } from "../employee";
import { EmployeeService } from "../employee.service";

@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.css"],
})
export class CreateComponent implements OnInit {
  @ViewChild("template", { read: TemplateRef, static: true }) // for notification
  newId: number;

  ESC_KEY = 27;

  @Output() xclose: EventEmitter<any> = new EventEmitter();

  width = "340";
  height = "auto";
  minHeight = "210";
  minWidth = "200";
  title = "Add a new employee";

  //Image file upload
  public myFiles: Array<any>;

  //Departments list
  public departmentList: Array<{ text: string; value: string }> = [
    { text: "Marketing", value: "Marketing" },
    { text: "Sales", value: "Sales" },
    { text: "IT", value: "IT" },
    { text: "Security", value: "Security" },
    { text: "Administration", value: "Administration" },
  ];

  // Images file upload restriction
  public FileTypeRestrictions: FileRestrictions = {
    allowedExtensions: [".jpg", ".png"],
    maxFileSize: 2000000,
    minFileSize: 5000,
  };

  @ViewChild(WindowComponent, { static: false })
  windowComponent: WindowComponent;

  /**
   * Centers Kendo-Window to the middle of the screen
   */
  @HostListener("window:scroll", [])
  @HostListener("window:resize", [])
  centerWindow() {
    const windowService = this.windowComponent["service"] as DragResizeService;

    if (!windowService) {
      return;
    }
    if (windowService.options.state === "maximized") {
      return;
    }

    const wnd = windowService.windowViewPort;
    const wrapper = offset(windowService.window.nativeElement);

    const top = Math.max(0, (wnd.height - wrapper.height) / 2);
    const left = Math.max(0, (wnd.width - wrapper.width) / 2);

    windowService.options.position = "fixed";
    this.windowComponent.setOffset("top", top);
    this.windowComponent.setOffset("left", left);
  }

  // @HostListener("keydown", ["$event"])
  // public onComponentKeydown(event: KeyboardEvent): void {
  //   // don't fix .keyCode to keep IE11 support
  //   // tslint:disable-next-line: deprecation
  //   if (event.keyCode === this.ESC_KEY) {
  //     this.windowComponent.close.emit();
  //   }
  // }

  private base64textString: String = "";
  encData: any;

  constructor(
    public employeeService: EmployeeService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.employeeService.getAll().subscribe((data: Employee[]) => {
      // console.error("CreateComponent:ngOnInit():data", data);
      let result = data.map((a) => a.id);
      let maxId = Math.max(...result);
      this.newId = maxId + 1;
      // console.error("CreateComponent:ngOnInit():this.newId", this.newId);
    });
  }

  public form: FormGroup = new FormGroup({
    id: new FormControl("", [Validators.required]),
    empFname: new FormControl("", [
      Validators.required,
      Validators.pattern("^[A-Za-z ]+$"),
    ]),
    empLname: new FormControl("", [
      Validators.required,
      Validators.pattern("^[A-Za-z ]+$"),
    ]),
    empDOB: new FormControl("", [Validators.required]),
    empSal: new FormControl("", [Validators.required]),
    empImg: new FormControl("", [Validators.required]),
    deptID: new FormControl("", [Validators.required]),
  });

  get f() {
    return this.form.controls;
  }

  handleFileSelect(evt) {
    // console.error("handleFileSelect(evt)");
    var files = evt;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    // console.error("readerEvt:");
    var binaryString = readerEvt.target.result;
    // console.error("readerEvt.target.result:");
    this.base64textString = btoa(binaryString);
    // console.error("encodedData:");
    this.form.value.empImg = this.base64textString;

    let date = JSON.stringify(this.form.value.empDOB);
    this.form.value.empDOB = JSON.parse(date).substring(0, 10);

    this.employeeService.create(this.form.value).subscribe((res) => {
      // console.log("Employee Added successfully!");
      this.windowComponent.close.emit();
      this.employeeService.showSuccess("Employee Added successfully!");
    });
  }
  onSubmit() {
    if (this.form.invalid) {
      console.error("invalid");
      this.form.markAllAsTouched();
      return;
    }
    // console.error("onSubmit()");
    this.handleFileSelect(this.form.value.empImg);
  }
}
