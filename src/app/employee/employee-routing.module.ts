import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { IndexComponent } from "./index/index.component";
import { ViewComponent } from "./view/view.component";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";

const routes: Routes = [
  { path: "", redirectTo: "employee/index", pathMatch: "full" },
  { path: "employee/index", component: IndexComponent },
  { path: "employee/:id/view", component: ViewComponent },
  { path: "employee/create", component: CreateComponent },
  { path: "employee/:id/edit", component: EditComponent },
];

// const routes: Routes = [
//   {
//     path: "",
//     component: IndexComponent,
//     children: [
//       {
//         path: "employee/index",
//         component: IndexComponent,
//       },
//       {
//         path: "employee/:id/view",
//         component: ViewComponent,
//       },
//       {
//         path: "employee/create",
//         component: CreateComponent,
//       },
//       {
//         path: "employee/:id/edit",
//         component: EditComponent,
//       },
//     ],
//   },
//   {
//     path: "",
//     redirectTo: "employee/index",
//     pathMatch: "full",
//   },
// ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeRoutingModule {}
